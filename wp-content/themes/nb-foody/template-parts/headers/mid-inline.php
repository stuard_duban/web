<div class="top-section-wrap">
    <div class="container">
        <div class="top-section">
            <?php $text_section_content = nbfoody_get_options('nbcore_header_text_section');
            if($text_section_content):
            ?>
            <div class="text-section">
                <?php echo esc_html($text_section_content); ?>
            </div>
            <?php
            endif; ?>
            <div class="icon-header-section">
                <div class="icon-header-wrap">
                <?php
                nbfoody_header_woo_section();
                nbfoody_search_section();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <div class="flex-section equal-section flex-end">
                <?php nbfoody_main_nav(); ?>
            </div>
            <div class="flex-section">
                <?php nbfoody_get_site_logo(); ?>
            </div>
            <div class="flex-section equal-section">
                <?php nbfoody_sub_menu();?>
            </div>
        </div>
    </div>
</div>