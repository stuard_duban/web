<?php
$text_section_content = nbfoody_get_options('nbcore_header_text_section');
$social_section_content = (nbfoody_get_options('nbcore_header_facebook') || nbfoody_get_options('nbcore_header_twitter') || nbfoody_get_options('nbcore_header_linkedin') || nbfoody_get_options('nbcore_header_instagram') || nbfoody_get_options('nbcore_header_blog') || nbfoody_get_options('nbcore_header_pinterest') || nbfoody_get_options('nbcore_header_ggplus'));
?>
<?php if($text_section_content || has_nav_menu('header-sub') || $social_section_content): ?>
	<div class="top-section-wrap">
		<div class="container">
			<div class="top-section">
				<?php $text_section_content = nbfoody_get_options('nbcore_header_text_section'); 
				if($text_section_content):
				?>
					<div class="text-section">
						<?php echo esc_html($text_section_content); ?>
					</div>
				<?php endif;
				if($social_section_content): ?>
					<div class="socials-section">
						<?php nbfoody_social_section(); ?>
					</div>
				<?php endif;
				if (has_nav_menu('header-sub')):
					nbfoody_sub_menu();
				endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="middle-section-wrap">
    <div class="container-fluid">
        <div class="middle-section">
            <?php nbfoody_get_site_logo(); ?>
            <div class="main-nav-wrap">
                <?php nbfoody_main_nav(); ?>
            </div>
            <div class="icon-header-section">
                <div class="icon-header-wrap">
                <?php 
                nbfoody_search_section(false);
                nbfoody_header_woo_section(false);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
