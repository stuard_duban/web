<?php

class NBFoody_Customize_Options_Pages
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Pages', 'nb-foody'),
            'priority' => 18,
            'sections' => apply_filters('nbt_pages_array', array(
                'page_general' => array(
                    'title' => esc_html__('General', 'nb-foody'),
                    'settings' => array(
                    ),
                    'controls' => array(
                    ),
                ),
            )),
        );
    }
}