<?php

class NBFoody_Customize_Options_Elements
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Elements', 'nb-foody'),
            'priority' => 12,
            'sections' => apply_filters('nbt_elements_array', array(
                'title_section_element' => array(
                    'title' => esc_html__('Title section', 'nb-foody'),
                    'settings' => array(
                        'show_title_section' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'home_page_title_section' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_page_title_size' => array(
                            'default' => '50',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_page_title_padding' => array(
                            'default' => '75',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        // TODO postMessage this
                        'nbcore_page_title_image' => array(
                            'default' => '',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_file_image')
                        ),
                        'nbcore_page_title_color_focus' => array(),
                    ),
                    'controls' => array(
                        'show_title_section' => array(
                            'label' => esc_html__('Show title section', 'nb-foody'),
                            'section' => 'title_section_element',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'home_page_title_section' => array(
                            'label' => esc_html__('Show Homepage title', 'nb-foody'),
                            'description' => esc_html__('Turn this off to not display the title section for only homepage', 'nb-foody'),
                            'section' => 'title_section_element',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_page_title_size' => array(
                            'label' => esc_html__('Font size', 'nb-foody'),
                            'section' => 'title_section_element',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '16',
                                'max' => '70',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_page_title_padding' => array(
                            'label' => esc_html__('Padding top and bottom', 'nb-foody'),
                            'section' => 'title_section_element',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '15',
                                'max' => '105',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_page_title_image' => array(
                            'label' => esc_html__('Image Background', 'nb-foody'),
                            'section' => 'title_section_element',
                            'type' => 'WP_Customize_Cropped_Image_Control',
                            'flex_width'  => true,
                            'flex_height' => true,
                            'width' => 2000,
                            'height' => 1000,
                        ),
                        'nbcore_page_title_color_focus' => array(
                            'section' => 'title_section_element',
                            'type' => 'NBFoody_Customize_Control_Focus',
                            'choices' => array(
                                'other_colors' => esc_html__('Edit color', 'nb-foody')
                            ),
                        ),
                    ),
                ),
                'button_element' => array(
                    'title' => esc_html__('Button', 'nb-foody'),
                    'settings' => array(
                        'nbcore_button_padding' => array(
                            'default' => '30',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_button_border_radius' => array(
                            'default' => '0',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_button_border_width' => array(
                            'default' => '2',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_button_padding' => array(
                            'label' => esc_html__('Padding left & right', 'nb-foody'),
                            'section' => 'button_element',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '5',
                                'max' => '60',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_button_border_radius' => array(
                            'label' => esc_html__('Border Radius', 'nb-foody'),
                            'section' => 'button_element',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '50',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_button_border_width' => array(
                            'label' => esc_html__('Border Width', 'nb-foody'),
                            'section' => 'button_element',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '1',
                                'max' => '10',
                                'step' => '1'
                            ),
                        ),
                    ),
                ),
                'share_buttons_element' => array(
                    'title' => esc_html__('Social share button', 'nb-foody'),
                    'settings' => array(
                        'share_buttons_style' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'share_buttons_position' => array(
                            'default' => 'inside-content',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                    ),
                    'controls' => array(
                        'share_buttons_style' => array(
                            'label' => esc_html__('Style','nb-foody'),
                            'section' => 'share_buttons_element',
                            'type' => 'select',
                            'choices' => array(
                                'style-1' => esc_html__('Style 1', 'nb-foody'),
                                'style-2' => esc_html__('Style 2', 'nb-foody'),
                            ),
                        ),
                        'share_buttons_position' => array(
                            'label' => esc_html__('Buttons position','nb-foody'),
                            'section' => 'share_buttons_element',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'inside-content' => get_template_directory_uri() . '/assets/images/options/ss-inside.png',
                                'floating' => get_template_directory_uri() . '/assets/images/options/ss-floating.png',
                            ),
                        ),
                    ),
                ),
                'pagination_element' => array(
                    'title' => esc_html__('Pagination', 'nb-foody'),
                    'settings' => array(
                        'pagination_style' => array(
                            'default' => 'pagination-style-1',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                    ),
                    'controls' => array(
                        //TODO Radio image this
                        'pagination_style' => array(
                            'label' => esc_html__('Style', 'nb-foody'),
                            'section' => 'pagination_element',
                            'type' => 'select',
                            'choices' => array(
                                'pagination-style-1' => esc_html__('Style 1','nb-foody'),
                                'pagination-style-2' => esc_html__('Style 2','nb-foody'),
                            ),
                        ),
                    ),
                ),
                'back_top_element' => array(
                    'title' => esc_html__('Back to top', 'nb-foody'),
                    'settings' => array(
                        'show_back_top' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'back_top_shape' => array(
                            'default' => 'circle',
                            'transport' => 'postMessage',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'back_top_style' => array(
                            'default' => 'light',
                            'transport' => 'postMessage',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                    ),
                    'controls' => array(
                        'show_back_top' => array(
                            'label' => esc_html__('Show button', 'nb-foody'),
                            'section' => 'back_top_element',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'back_top_shape' => array(
                            'label' => esc_html__('Show button', 'nb-foody'),
                            'section' => 'back_top_element',
                            'type' => 'select',
                            'choices' => array(
                                'circle' => esc_html__('Circle','nb-foody'),
                                'square' => esc_html__('Square','nb-foody'),
                            ),
                        ),
                        'back_top_style' => array(
                            'label' => esc_html__('Show button', 'nb-foody'),
                            'section' => 'back_top_element',
                            'type' => 'select',
                            'choices' => array(
                                'light' => esc_html__('Light','nb-foody'),
                                'dark' => esc_html__('Dark','nb-foody'),
                            ),
                        ),
                    ),
                ),
            )),
        );
    }
}