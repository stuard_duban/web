<?php
class NBFoody_Customize_Options_Footer
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Footer', 'nb-foody'),
            'priority' => 17,
            'settings' => array(
                'nbcore_footer_top_intro' => array(),
                'nbcore_show_footer_top' => array(
                    'default' => false,
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                ),
                'nbcore_footer_top_layout' => array(
                    'default' => 'layout-9',
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                ),
                'nbcore_footer_bot_intro' => array(),
                'nbcore_show_footer_bot' => array(
                    'default' => false,
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                ),
                'nbcore_footer_bot_layout' => array(
                    'default' => 'layout-9',
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                ),
                'nbcore_footer_abs_intro' => array(),
                'nbcore_footer_abs_left_content' => array(
                    'default' => '',
                    'sanitize_callback' => 'wp_filter_nohtml_kses'
                ),
                'nbcore_footer_abs_right_content' => array(
                    'default' => '',
                    'sanitize_callback' => 'wp_filter_nohtml_kses'
                ),
                'nbcore_footer_abs_padding' => array(
                    'default' => '10',
                    'transport' => 'postMessage',
                    'sanitize_callback' => 'absint'
                ),
                'nbcore_footer_color_focus' => array(),
            ),
            'controls' => array(
                'nbcore_footer_top_intro' => array(
                    'label' => esc_html__('Footer top section', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Heading',
                ),
                'nbcore_show_footer_top' => array(
                    'label' => esc_html__('Show this section', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Switch',
                ),
                'nbcore_footer_top_layout' => array(
                    'label' => esc_html__('Columns', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Radio_Image',
                    'choices' => array(
                        'layout-1' => get_template_directory_uri() . '/assets/images/options/footers/footer-1.png',
                        'layout-2' => get_template_directory_uri() . '/assets/images/options/footers/footer-2.png',
                        'layout-3' => get_template_directory_uri() . '/assets/images/options/footers/footer-3.png',
                        'layout-4' => get_template_directory_uri() . '/assets/images/options/footers/footer-4.png',
                        'layout-5' => get_template_directory_uri() . '/assets/images/options/footers/footer-5.png',
                        'layout-6' => get_template_directory_uri() . '/assets/images/options/footers/footer-6.png',
                        'layout-7' => get_template_directory_uri() . '/assets/images/options/footers/footer-7.png',
                        'layout-8' => get_template_directory_uri() . '/assets/images/options/footers/footer-8.png',
                        'layout-9' => get_template_directory_uri() . '/assets/images/options/footers/footer-9.png',
                    ),
                ),
                'nbcore_footer_bot_intro' => array(
                    'label' => esc_html__('Footer bottom section', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Heading',
                ),
                'nbcore_show_footer_bot' => array(
                    'label' => esc_html__('Show this section', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Switch',
                ),
                'nbcore_footer_bot_layout' => array(
                    'label' => esc_html__('Columns', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Radio_Image',
                    'choices' => array(
                        'layout-1' => get_template_directory_uri() . '/assets/images/options/footers/footer-1.png',
                        'layout-2' => get_template_directory_uri() . '/assets/images/options/footers/footer-2.png',
                        'layout-3' => get_template_directory_uri() . '/assets/images/options/footers/footer-3.png',
                        'layout-4' => get_template_directory_uri() . '/assets/images/options/footers/footer-4.png',
                        'layout-5' => get_template_directory_uri() . '/assets/images/options/footers/footer-5.png',
                        'layout-6' => get_template_directory_uri() . '/assets/images/options/footers/footer-6.png',
                        'layout-7' => get_template_directory_uri() . '/assets/images/options/footers/footer-7.png',
                        'layout-8' => get_template_directory_uri() . '/assets/images/options/footers/footer-8.png',
                        'layout-9' => get_template_directory_uri() . '/assets/images/options/footers/footer-9.png',
                    ),
                ),
                'nbcore_footer_abs_intro' => array(
                    'label' => esc_html__('Absolute Footer', 'nb-foody'),
                    'description' => esc_html__('These area take text and HTML code for its content', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Heading',
                ),
                'nbcore_footer_abs_left_content' => array(
                    'label' => esc_html__('Left content', 'nb-foody'),
                    'type' => 'textarea',
                    'section' => 'footer'
                ),
                'nbcore_footer_abs_right_content' => array(
                    'label' => esc_html__('Right content', 'nb-foody'),
                    'type' => 'textarea',
                    'section' => 'footer'
                ),
                'nbcore_footer_abs_padding' => array(
                    'label' => esc_html__('Padding top and bottom', 'nb-foody'),
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Slider',
                    'choices' => array(
                        'unit' => 'px',
                        'min' => '5',
                        'max' => '60',
                        'step' => '1',
                    ),
                ),
                'nbcore_footer_color_focus' => array(
                    'section' => 'footer',
                    'type' => 'NBFoody_Customize_Control_Focus',
                    'choices' => array(
                        'footer_colors' => esc_html__('Edit color', 'nb-foody'),
                    ),
                ),
            ),
        );
    }
}