<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of my-account
 *
 * @author Windows
 */
class myAccount {

    protected $Order;
    protected $Plan;
    protected $WCOrder;
    protected $ID_User;
    protected $time;
    protected $day1;
    protected $day2;
    protected $Products;


    /**/
    protected $DeliveryDates;
    protected $FirstDay;
    protected $SecondDay;
    protected $Hour;
    protected $IS;
    protected $MODE;
    protected $Edit;

//put your code here
    public function __construct() {
        $User = wp_get_current_user();
        if (isset($_REQUEST['order'])) {
            $this->ID_User = $User->ID;
            $this->Order = $_REQUEST['order'];

            // Version que funciona
            $this->WCOrder = WC_Order_in_last_item(array('orders' => $this->Order, 'user' => $this->ID_User));

            //echo "WC order: <pre>" . var_dump($this->WCOrder);
            $this->Plan = zWhatPlans($this->Order);

            if (isset($this->WCOrder[0])) {
                $this->WCOrder = $this->WCOrder[0];
            }
            $this->Edit = isset($_REQUEST['edit']);
        }

        //ID DEL PRODUCTO PADRE
        $plan = 0;
        //NUMERO DE PLATOS EN EL PLAN
        $name = 0;

        $this->Products = [];

        $auto = true;
        $orden = wc_get_order( $this->Order );

        //echo "Orden del woocommerce: <pre>" . var_dump($orden->get_items());exit;
        //echo "Orden: <pre>" . var_dump($this->WCOrder);exit;
        $time = str_replace('You receive your order this', '', $this->WCOrder->tim);
        $time = str_replace('You will receive half of your meals on:', '', $time);
        $time = str_replace(' And the other half on: ', ' and ', $time);
        $time = str_replace('<strong>', '', $time);
        $time = str_replace('</strong>', '', $time);
        $time = str_replace('<br>', '', $time);
        $this->time = explode(' and ', $time);
        if ($this->WCOrder->products !== '-') {
            $it = explode('*', $this->WCOrder->products);
            foreach ($it as $t) {
                $t = explode(',', $t);
                if (count($t) > 1) {
                    $type = 'Bebidas';
                    if (strpos($t[1], 'Subscription') !== false) {
                        $type = 'Subscription';
                    }
                    $this->Products[] = array(
                        'product_id' => $t[0],
                        '_type' => $type,
                        'name' => $t[1],
                        'quantity' => $t[2]
                    );
                }
            }
        }
        if (count($this->time) > 1) {
            $this->day1 = str_replace(' ', '', explode(',', $this->time[0])[0]);
            $this->day2 = str_replace(' ', '', explode(',', $this->time[1])[0]);
        }

        $this->DeliveryDates = [];
        if ($this->WCOrder->delivery !== '-') {
            foreach (explode(',', $this->WCOrder->delivery) as $t) {
                $t = explode('=', $t);
                if (isset($t[0]) && isset($t[1])) {
                    $this->DeliveryDates[$t[0]] = $t[1];
                }
            }
        }
        $this->IS = ' AND ';
        if (count($this->time) === 1) {
            $this->IS = ' OR ';
            if ($this->Edit) {
                $this->Edit = false;
            }
            if ($this->WCOrder->city === 'Palm Beach County') {
                $this->Hour = '10:00 AM ~ 08:00 PM';
                if (strpos($this->time[0], 'Sunday') !== false) {
                    $this->FirstDay = 'Sunday';
                    $this->SecondDay = 'Wednesday';
                    $this->MODE = '+';
                }
                if (strpos($this->time[0], 'Wednesday') !== false) {
                    $this->FirstDay = 'Wednesday';
                    $this->SecondDay = 'Sunday';
                    $this->MODE = '-';
                }
            }
            if ($this->WCOrder->city === 'Broward and Martin County') {
                $this->Hour = '06:00 PM ~ 10:00 PM';
                if (strpos($this->time[0], 'Monday') !== false) {
                    $this->FirstDay = 'Monday';
                    $this->SecondDay = 'Thursday';
                    $this->MODE = '+';
                }
                if (strpos($this->time[0], 'Thursday') !== false) {
                    $this->FirstDay = 'Thursday';
                    $this->SecondDay = 'Monday';
                    $this->MODE = '-';
                }
            }
            $date213 = str_replace($this->FirstDay . ', ', '', str_replace(' ' . $this->Hour, '', $this->time[0]));
            foreach (array('January' => '01', 'February' => '02', 'March' => '03', 'April' => '04', 'May' => '05', 'June' => '06',
        'July' => '07', 'August' => '08', 'September' => '09', 'October' => '10', 'Novembre' => '11', 'December' => '12') as $key => $value) {
                $date213 = str_replace($key . ' ', $value . '/', $date213);
            }
            $date213 = str_replace(', ', '/', $date213);
            $dayin12 = [0 => 'Saturday'];
            $lolico = 0;
            while ($dayin12[0] !== $this->SecondDay) {
                //['Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday']
                $date213 = new DateTime($date213);
                $dayin12 = explode(',', date('l, m/d/Y', strtotime($this->MODE . '1 day', strtotime(date('Y-m-d', strtotime($date213->format('Y-m-d')))))));
                $date213 = $dayin12[1];
                $lolico++;
                if ($lolico > 90) {
                    break;
                }
            }
            $dayin12 = new DateTime($dayin12[1]);
            $dayin12 = $dayin12->format('l, F d, Y');
            $dayin12 .= ' ' . $this->Hour;
            $this->time[1] = $dayin12;
        }
    }

    public function init() {

    }

    public function styles() {
        $r = '<style>';
        $r .= '.ViewOrdersMeals{text-align: center;max-width: 900px;margin: 0 auto;}';
        $r .= '.more {display: none;margin: -60px auto;position: absolute;border: 3px solid #00f100;max-width: 100px;max-height: 100px;z-index: 999;}';
        $r .= '.picture:hover .more {display: block;}';
        $r .= '.type_item_Bebidas {color: #3d3d3e;font-weight: bold;}';
        $r .= '.more img{
                min-width: 100px !important;
                min-height: 100px !important;
            }
            .nb-page-title-wrap{display:none;}
            .more_img{display:block;}
            .more{display:none;}
            .picture:hover .more {
                display: block;
                position: absolute;
                margin-top: -28px;
                margin-left: -25px;
                /*border-radius: 50%;*/
                overflow: hidden;
                border: 3px solid #2f8e16;
                min-width: 100px;
                min-height: 100px;
            }
            .picture:hover .more_img{display:none;}
            tr:hover{
                background-color: #dcedc8;
            }
            @media (max-width: 400px){
                a.btn-sm {display: block;margin-bottom: 3px;text-align: center;}
            }';
        $r .= '</style>';
        return $r;
    }

    public function header($title) {
        $r = '<h1>' . $title . '</h1>';
        $r .= '<div style="text-align:  center;margin-top: -25px;font-size: 80px;line-height: 30px;"><span class="IdealNutrition in-ideal" style=""></span></div>';
        return $r;
    }

    public function viewMealsOrders() {
        //var_dump();
        $r = $this->styles();
        $r .= $this->header('Order Meals for Your Subscription Plan');
        //$r .='<form action="'. $_SERVER['SCRIPT_URL'].'" method="post">';
        //$r .='<input type="hidden" name="order" value="'. $this->Order.'">';
        //$r .='<input type="hidden" name="user" value="'. $this->ID_User .'">';
        $r .= '<div style="text-align: left;">';
        $r .= '<a style="margin-right: 5px;" href="javascript:void()" onclick="javascript:history.back()" class="btn btn-default btn-sm pull-left">< Back</a>';
        $r .= '<a style="margin-right: 5px;" href="' . zUriMyAccount() . '" class="btn btn-primary btn-sm pull-left">Return to Dashboard</a>';
        $r .= '<a style="margin-right: 5px;" href="/' . zpage_by_title(PAGE_SELECT_PRODUCTS) . '/?order=' . $this->Order . '" class="btn btn-warning btn-sm pull-left">Edit Meal Selection</a>';
        $r .= '</div>';
//$r .= '<a style="display: none !important" href="/my-account/view-order/ $ID_Order" class="btn btn-info btn-sm pull-left" style="margin: 9px 5px 38px;">View Order # $ID_Order</a>';

        $r .= '<table cellspacing="0" cellpadding="0"  style="position:relative;width: 100%; margin: 20px auto;">
                <thead>
                    <tr>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number">
                            #
                        </th>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                            Preview
                        </th>
                        <th style="text-align: left;font-weight: 900;" class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                            Product
                        </th>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                            Quantity
                        </th>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody>';
?>
        <?php

        $WC = new WC_Product_Factory();
        $icount = 0;
        $Limit = 0;
        $_ = [];
        foreach ($this->time as $value) {
            $value = explode(',', str_replace(' ', '', $value));
            $_[] = $value[0];
        }
        //var_dump($_);
        $j = '';
        foreach ($this->Products as $k => $item) {
            $product = $WC->get_product($item['product_id']);
            if (intval($item['product_id']) !== intval($plan)) {
                $quantity = ($item['quantity']);
                $icount++;
                $j .= '<tr class="type_item_' . $item['_type'] . '">';
                $j .= '<td><?= $icount; ?></td>';
                $j .= '<td>';
                $j .= '<div class="picture">';
                $j .= '<div class="more_img" style="max-width: 24px;margin: 0 auto;"><i class="icon-search"></i></div>';
                $j .= '<div class="more">' . $product->get_image('shop_thumbnail') . '</div>';
                $j .= '</div>';
                $j .= '</td>';
                $j .= '<td style="text-align: left;font-weight: 900;">' . $item['name'] . '</td>';
                $j .= '<td>' . $quantity . '</td>';
                $j .= '<td>';
                if (floatval($product->get_price()) > 0) {
                    $j .= '$' . number_format((floatval($quantity) * floatval($product->get_price())), 2, '.', ',');
                } else {
                    $j .= '-';
                }
                $j .= '</td>';
                $j .= '</tr>';
            }
        }
        $r .= $j;
        $r .= '</tbody>';
        $r .= '</table>';
        return '<div class="ViewOrdersMeals">' . $r . '</div><br><br><br>';
    }

    public function ViewMealsDelivery() {
        $r = $this->styles();
        $r .= $this->header('Order Meals for Your Subscription Plans');
        $r .= '<div style="text-align: left;">';
        $r .= '<h2>' . $this->Plan['title'] . '</h2>';
        $r .= '<h3>Delivery for <b>' . $this->WCOrder->city . '</b></h4>';
        $r .= '<h4>';
        if (isset($this->time[0])) {
            $r .= '<b>' . $this->time[0] . '</b>';
        }
        if (isset($this->time[1]) && $this->IS != ' OR ') {

            $r .= $this->IS . '<b>' . $this->time[1] . '</b>';
        }
        $r .= '</h4></div>';
        $r .= $this->form_delivery_date();
        return '<div class="ViewOrdersMeals">' . $r . '</div><br><br><br>';
    }

    public function form_delivery_date() {
        $r = '<form class="formCSS" action="' . zUriMyAccount() . '" method="post">';
        $r .= '<input type="hidden" name="subscription_action" value="UpdateDeliveryDate">';
        $r .= '<input type="hidden" name="order" value="' . $this->Order . '">';
        $r .= '<input type="hidden" name="user" value="' . $this->ID_User . '">';
        $r .= '<div style="margin-top: 25px;text-align:  left;margin-bottom: 20px;"><a style="margin-right: 5px;" href="javascript:void()" onclick="javascript:history.back()" class="btn btn-default btn-sm pull-left">< Back</a>';
        $r .= '<a style="margin-right: 5px;" href="' . zUriMyAccount() . '" class="btn btn-primary btn-sm pull-left">Return to Dashboard</a>';
        if ($this->Edit) {
            $r .= '<input id="btn_save" type="submit" value="UPDATE DELIVERY DATES" class="movile btn btn-danger btn-sm pull-left" ></div>';
            $r .= '<div style="display:inline-block;float: left;max-width: 360px;min-width: 360px;">';
            $r .= '<h5 id="lh5l" style="font-weight: bold;">You must select <span class="_count">6</span> meal for each day</h5>';
            $r .= '</div>';
            $r .= '<div style="display:inline-block;float: right;max-width: 360px;min-width: 360px;">';
            $r .= '<h5 style="text-align: right;"><strong>' . $this->day1 . ':</strong> <span class="_' . $this->day1 . '_ reset">0</span>/<span class="_count">0</span> <strong>' . $this->day2 . ':</strong> <span class="_' . $this->day2 . '_ reset">0</span>/<span class="_count">0</span></h5>';
            $r .= '</div>';
        } else {
            if (isset($this->time[1]) && $this->IS != ' OR ') {
                $r .= '<a href="/' . zpage_by_title(PAGE_VIEW_MEALS_OF_DELIVERY) . '/?order=' . $this->Order . '&edit" class="movile btn btn-warning btn-sm pull-left">Change Delivery Dates</a></div>';
            } else {
                $r .= '</div>';
            }
        }
        $r .= '<div class="tab-responsive">
            <table cellspacing="0" cellpadding="0"  style="position:relative;width: 100%; margin:  auto;">
                <thead>
                    <tr>
                    <!--View info of deliveries-->
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number">
                            #
                        </th>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                            Preview
                        </th>
                        <th style="text-align: left;" class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                            Product
                        </th>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                            Quantity
                        </th>
                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                            Day
                        </th>
                    </tr>
                </thead>
                <tbody>';
        $WC = new WC_Product_Factory();
        $icount = 0;
        $Limit = 0;
        $_ = [];
        foreach ($this->time as $value) {
            $value = explode(',', str_replace(' ', '', $value));
            $_[] = $value[0];
        }
        $_COUNTS_ = 0;
        $Temps4523 = '';
        $j = '';
        //echo "Prodcutos: <pre>" . var_dump($this->Products);
        foreach ($this->Products as $k => $item) {
            $_COUNTS_++;
            $product = $WC->get_product($item['product_id']);
            if (intval($item['product_id']) !== intval($this->Plan['id'])) {
                $quantity = ($item['quantity']);
                $icount++;
                if (strpos($item['name'], 'Subscription') !== false) {
                    $class = 'type_item_Suscription';
                } else {
                    if (isset($DUTI[$item['product_id']])) {
                        $class = 'type_item_Bebidas';
                    } else {
                        $class = 'type_item_none';
                    }
                }
                //var_dump($item['name']);
                $j .= '<tr class="' . $class . '">';
                $j .= '<td>' . $icount . '</td>';
                $j .= '<td>
                                    <div class="picture">
                                        <div class="more_img" style="max-width: 24px;margin: 0 auto;"><i class="icon-search"></i></div>';
                $j .= '<div class="more" style="margin: -40px auto;">' . $product->get_image('shop_thumbnail') . '</div>';
                $j .= '</div>
                                </td>
                                <td style="text-align: left;">' . $item['name'] . '</td>';
                $j .= '<td>1</td>
                                <td>';
                if (isset($_REQUEST['edit'])) {
                    $Extra = false;
                    $j .= '<select';
                    if (strpos($item['name'], 'Subscription') !== false) {
                        $j .= ' name="' . $_COUNTS_ . 'time' . $item['product_id'] . '" data-type="Subscription" style="max-height:  28px;font-size:  13px;"';
                        $Limit++;
                    } else {
                        $j .= ' data-type="Extra" disabled style="max-height: 28px;font-size: 13px;background: transparent;border: none;"';
                        $Extra = true;
                    }
                    $j .= ' class="actions_select">';
                    $a = '';
                    foreach ($_ as $value) {
                        $a .= '<option value="' . $value . '">' . $value . '</option>';
                    }
                    $j .= $a;
                    $j .= '</select>';

                    if ($Extra) {
                        $j .= '<input type="hidden" name="' . $_COUNTS_ . 'time' . $item['product_id'] . '" value="' . $this->day1 . '">';
                    }
                } else {
                    if (isset($this->DeliveryDates[$_COUNTS_ . $item['product_id']])) {
                        if (strlen(trim($Temps4523)) === 0) {
                            $Temps4523 = $this->DeliveryDates[$_COUNTS_ . $item['product_id']];
                        }

                        if ($this->IS === ' OR ') {
                            $j .= $Temps4523;
                        } else {
                            $j .= $this->DeliveryDates[$_COUNTS_ . $item['product_id']];
                        }
                    } else {
                        $j .= explode(',', $this->time[0])[0];
                    }
                }
                $j .= '</td>
                </tr>';
                //echo '<input type="hidden" value="'.$item['quantity'].'">';
                for ($index = 1; $index < $quantity; $index++) {
                    $_COUNTS_++;
                    $icount++;
                    $j .= '<tr class="' . $class . '">';
                    $j .= '<td>' . $icount . '</td>
                        <td>
                            <div class="picture">
                                <div class="more_img" style="max-width: 24px;margin: 0 auto;"><i class="icon-search"></i></div>
                                <div class="more" style="margin: -40px auto;">' . $product->get_image('shop_thumbnail') . '</div>
                            </div>
                        </td>
                        <td style="text-align: left;">' . $item['name'] . '</td>
                        <td>1</td>
                        <td>';

                    if ($this->Edit) {
                        $Extra = false;
                        $j .= '<select';
                        if (strpos($item['name'], 'Subscription') !== false) {
                            $j .= ' name="' . $_COUNTS_ . 'time' . $item['product_id'] . '" data-type="Subscription" style="max-height:  28px;font-size:  13px;"';
                            $Limit++;
                        } else {
                            $j .= ' data-type="Extra" disabled style="max-height: 28px;font-size: 13px;background: transparent;border: none;"';
                            $Extra = true;
                        }
                        $j .= ' class="actions_select">';
                        $a = '';
                        foreach ($_ as $value) {
                            $a .= '<option value="' . $value . '">' . $value . '</option>';
                        }
                        $j .= $a;
                        $j .= '</select>';
                        if ($Extra) {
                            $j .= '<input type="hidden" name="' . $_COUNTS_ . 'time' . $item['product_id'] . '" value="' . $this->day1 . '">';
                        }
                    } else {
                        $Temps4523 = '';
                        if (isset($this->DeliveryDates[$_COUNTS_ . $item['product_id']])) {
                            $j .= $this->DeliveryDates[$_COUNTS_ . $item['product_id']];
                            $Temps4523 = $this->DeliveryDates[$_COUNTS_ . $item['product_id']];
                        } else {

                            if (isset($this->Products[$item['product_id'] . '_' . $icount])) {
                                $arg24523 = $this->Products[$item['product_id'] . '_' . $icount];
                            } else {
                                $arg24523 = explode(',', $this->time[0])[0];
                            }
                            if ($Temps4523 === $arg24523) {
                                $j .= $arg24523;
                            } else {
                                $j .= $Temps4523;
                            }
                        }
                    }
                    $j .= '</td>
                    </tr>';
                }
            }
        }
        $r .= $j;
        $r .= '</tbody>
        </table>
        </div>
        </form>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var _ = ' . json_encode($_) . ';';
        $r .= 'var LL = true;';
        $r .= 'var FL = 0;';
        if ($this->IS === ' AND ') {
            if ($Limit%2==0){
                $r .= 'var limit = ' . ($Limit / 2) . ';';
            }else{
                $r .= 'var limit = ' . (intval(($Limit / 2))+1) . ';';
                $r .= 'LL = false;';
            }
        } else {
            $r .= 'var limit = ' . $Limit . ';';
        }
        $r .= 'var btn = jQuery("#btn_save");
                var count = 1;
                var very = function () {';
        if ($this->IS === ' AND ') {
            $r .= 'var avance = true;

                        jQuery("span.reset").each(function () {
                            if (parseInt(jQuery(this).html()) !== limit) {
                                avance = false;
                            }
                        });
                        if(!LL){
                            avance = true;
                            FL = limit;
                            jQuery("span.reset").each(function (e) {
                                if (parseInt(jQuery(this).html()) !== FL) {
                                    avance = false;
                                }
                                FL = (limit-1)
                            });
                        }
';
        } else {
            $r .= 'var avance = false;
                        jQuery("span.reset").each(function () {
                            if (parseInt(jQuery(this).html()) === limit) {
                                avance = true;
                            }
                        });';
        } $r .= 'return avance;
                };
                if (btn) {
                    setInterval(function () {
                        if (very()) {
                            btn.removeAttr("disabled");';
        $r .= '} else {
                            console.log(btn.html());
                            btn.attr("disabled", "");
                        }
                    }, 500);
                }
                if(!LL){jQuery("#lh5l").remove();}
                FL = limit;
                jQuery("span._count").each(function (e) {
                    console.log(e);

                    jQuery(this).html(FL);
                    if(!LL){
                        FL = (limit-1);
                    }
                });';
        $r .= 'jQuery(".actions_select[data-type=\"Subscription\"]").each(function () {
                    jQuery(this).change(function () {
                        jQuery("span.reset").each(function () {
                            jQuery(this).html("0");
                        });
                        jQuery(".actions_select[data-type=\"Subscription\"]").each(function () {
                            var spn = jQuery("span._" + jQuery(this).val() + "_");
                            spn.html((parseInt(spn.html()) + 1));
                        });
                    });
                });';
        if (!$this->Edit) {
            $r .= 'jQuery("[data-type=\"Subscription\"]").each(function () {
                        var p = jQuery(this).parent();
                        var name = jQuery(this).attr("name");
                        p.html("");
                        if (parseInt(count) > parseInt(limit)) {
                            p.append(_[1]);
                            p.append("<input name=\"" + name + "\" type=\"hidden\" value=\"" + _[1] + "\">");
                        } else {
                            p.append(_[0]);
                            p.append("<input name=\"" + name + "\" type=\"hidden\" value=\"" + _[0] + "\">");
                        }
                        count++;
                    });
                    jQuery("[data-type=\"Extra\"]").each(function () {
                        var p = jQuery(this).parent();
                        var name = jQuery(this).attr("name");
                        p.html("");
                        p.append(_[0]);
                        p.append("<input name=\"" + name + "\" type=\"hidden\" value=\"" + _[0] + "\">");
                        count++;
                    });';
            if (count($this->Products) === 0) {
                $r .= 'if (jQuery("input#btn_save")) {
                            jQuery("input#btn_save").click();
                        }';
            }
        }
        $r .= '});
        </script>';
        return $r;
    }

}
