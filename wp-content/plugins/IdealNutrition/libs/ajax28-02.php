<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function notify_button_click() {
    // Check parameters
    $C = new zCuponez();
    $R = $C->getcoupon();
    $meals = 0;
    if ($R) {

        switch ($R->status) {
            case 'Available': $message = 'Coupon code ' . $R->status;
                $meals = $R->meals;
                break;
            case 'Disabled': $message = 'Coupon code ' . $R->status;
                break;
            case 'Locked': $message = 'Coupon code ' . $R->status;
                break;
            case 'Used': $message = 'Coupon code ' . $R->status;
                break;
            default: $message = 'Coupon code not available';
                break;
        }
    } else {
        $message = 'Coupon code not available';
    }
    wp_send_json(array('message' => $message, 'meals' => $meals));
}

function update_tax($data) {
    global $woocommerce;
    $percentage = 0.07;
    $taxes = array_sum($woocommerce->cart->taxes);
    $surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total + $taxes ) * $percentage;

    WC()->session->set('ship_tax', '7%');//WC()->session->get('ship_tax')
    WC()->cart->add_fee('Tax ', $surcharge, false, '');
}

function update_fee($data) {
    $Checked = _zCheckCart();
    echo "Esta imprimiendo algo: " . $_POST['Semana_Siguiente'];
    if ($Checked) {
        $isSub = $Checked['isSubscriptions'];

    } else {
        $isSub = false;
        if($_POST['Semana_Siguiente']){
          $isSub = true;
          //echo "esta entrando aqui";
        }
    }
    if (!isset($_POST['time'])) {

        $chosen = WC()->session->get('ship_val');
        $chosen = empty($chosen) ? true : false;

        if ($chosen) {
            if ($isSub) {
                WC()->session->set('ship_val', '0');
            } else {
                WC()->session->set('ship_val', '0.00');
            }
        }
        WC()->cart->add_fee('Delivery ', WC()->session->get('ship_val'));
    } else {
        if ($isSub) {
            WC()->session->set('ship_val', '0');
            $r = 'price 0';
        } else {
            $time = isset($_POST['time']) ? $_POST['time'] : '';
            if (strlen(trim($time)) > 0) {
                $time = explode(' And ', $time);
            }
            if (count($time) > 1) {
                WC()->session->set('ship_val', '10.00');
                $r = 'price 10';
            } else {
                WC()->session->set('ship_val', '5.00');
                $r = 'price 5';
            }
        }
        WC()->cart->add_fee('Delivery ', WC()->session->get('ship_val'));
        wp_send_json(array('message' => __(time(), 'wpduf')));
    }
}
