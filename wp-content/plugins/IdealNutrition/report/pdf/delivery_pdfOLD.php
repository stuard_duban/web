<?php
$data = file_get_contents('https://'.$_SERVER['SERVER_NAME'].'/wp-content/plugins/IdealNutrition/report/json/delivery.json');
$data = json_decode($data, true);
/*$_HTML_ = '<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/bootstrap-table/src/bootstrap-table.css">
        <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
        <link rel="stylesheet" href="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/examples.css">
        <script src="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/jquery.min.js"></script>
        <script src="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
        <![endif]-->
        <style>
            @font-face {
                font-family: "MankSans";
                src: url("https://ideal.luxlifeentertainment.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.eot");
                src: local("☺"), url("https://ideal.luxlifeentertainment.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.woff") format("woff"), url("https://ideal.luxlifeentertainment.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.ttf") format("truetype"), url("https://ideal.luxlifeentertainment.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.svg") format("svg");
                font-weight: normal;
                font-style: normal;
            }
            body, h1, table, tr, td, th{font-family: "MankSans" !important;}
        </style>
    </head>
    <body>
        <div class="container">';*/

        $_HTML_ = '<!DOCTYPE html>
<html style="margin: 0;">
    <head></head>
    <body style="margin: 45mm 8mm 2mm 8mm;">
        <div class="container">';
            
    $Orders= '';
    $totals = 0;
    foreach ($data as $key => $e) {
        $table1 = '';
        $table1 .= '<table cellpadding="0" cellspacing="0" style="width: 100%;border: 1px solid #1a1a1a75;">
                <thead>
                    <tr style="color: #353535;border-bottom: 1px solid #969696;background: #f4f4f4;">
                        <th style="padding:  5px;text-align: center;white-space: nowrap;">REF</th>
                        <!--th style="padding:  5px;text-align: center;">IMAGEN</th-->
                        <th style="padding:  5px;text-align: center;white-space: nowrap;">NAME OF MEALS FOR ORDER #['.$e['id'].']</th>
                        <th style="padding:  5px;text-align: center;white-space: nowrap;">QUANTITY</th>
                        <th style="padding:  5px;text-align: center;white-space: nowrap;">DAY</th>
                        <th style="padding:  5px;text-align: center;white-space: nowrap;">DELIVERY DATE</th>
                    </tr>
                </thead>
                <tbody>';
        $rw ='';
        $totals = 0;
        foreach ($e['Meals'] as $item) {
            $totals = intval($totals) + intval($item['qty']);
            $custom = isset($item['custom']) ? $item['custom']: $item['name'];
            $custom = strlen(trim($custom))===0 ?  $item['name']: $custom;
            $rw .= '<tr style="border-top: 1px solid #969696;">
                            <td style="white-space: nowrap;text-align:  center;color: #191919;font-weight:  bold;border-right: 1px solid #969696;">' . $item['sku'] . '</td>
                            <td style="font-weight: 500;padding-left: 15px;border-left: 1px solid #969696;border-right: 1px solid #969696;text-align:  left;">' .$custom. '</td>
                            <td style="text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $item['qty'] . '</td>
                            <td style="white-space: nowrap;text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $item['day'] . '</td>
                            <td style="white-space: nowrap;text-align:  center;font-size:  18px;">' . $item['date'] . '</td>
                        </tr>';
        }
        $table1 .=$rw;
        $table1 .= '</tbody>
                    </table>';
        $table2 = '';
        $table2 .= '<table cellpadding="0" cellspacing="0" style="width: 100%;border: 1px solid #1a1a1a75;">
                <thead>
                    <tr style="color: #353535;border-bottom: 1px solid #969696;background: #f4f4f4;">
                        <th style="padding:  5px;text-align: center;white-space: nowrap;">DETAILS</th>
                    </tr>
                </thead>
                <tbody>';
        $table2 .='<tr style="border-top: 1px solid #969696;">
                <td style="font-weight: 500;padding-left: 15px;border-left: 1px solid #969696;border-right: 1px solid #969696;text-align:  left;">' . 
                /*'<strong>Nº ORDER: #'.'</strong>'.$e['id'].'<br>'.
                '<strong>CUSTOMER: </strong>'.$e['Name'].'<br>'.
                '<strong>MAIL: </strong>'.$e['mail'].'<br>'.
                '<strong>PHONE: </strong>'.$e['phone'].'<br><hr>'.*/
                '<strong>ADDRESS: </strong>'.$e['Address'].'<br><hr>'.
                '<strong>CITY: </strong>'.$e['city'].'<br>'.
                '<strong>DELIVERY DATE: </strong>'. str_replace('<br>', '', $e['date']).'<br>'.
                
                '<strong>DELIVERY INSTRUCTIONS/SPECIAL REQUESTS: </strong>'.$e['notes']
                .'</td>
                </tr>';
        $table2 .= '</tbody>
                    </table>';
        
        $Orders .= '<div style="page-break-after: always;"></div> 
                <table width="100%" style="width: 100% !important;">
                    <tr>
                        <td style="width: 33.333%;">
                            <img style="display:inline-block;width: 150px;max-width: 150px;" src="https://ideal.luxlifeentertainment.com/wp-content/uploads/2018/03/Logo-ideal-Nutritions-1.png" alt="IDEAL NUTRITION">                                    
                        </td>
                        <td style="width: 33.333%;">
                            <h1 style="display:inline-block;vertical-align:text-top;font-size:  30px;line-height: 34px;text-transform:  uppercase;margin-top: 46px;margin-left: 20px;">Delivery Report</h1>
                        </td>
                        <td style="text-align: right;width: 33.333%;">Printing Date: '.date('l, d, m, Y h:m a').'</td>
                    </tr>
                </table>
                <table width="100%" style="width: 100% !important;"><tr><td><table cellpadding="0" cellspacing="0" style="width: 100%;border: 1px solid #1a1a1a75;">
                        <thead>
                            
                            <tr style="color: #353535;background: #36ff00e0;">
                                <th style="padding:  5px;text-align: center;">ORDEN</th>
                                <th style="padding:  5px;text-align: center;">CUSTOMER</th>
                                <th style="padding:  5px;text-align: center;">MAIL</th>
                                <th style="padding:  5px;text-align: center;">PHONE</th>
                                <th style="padding:  5px;text-align: center;">CITY</th>
                                <th style="padding:  5px;text-align: center;">TOTAL ITEMS</th>
                            </tr>
                        </thead>
                        <tbody>';
        $Orders .= '<tr style="border-top: 1px solid #969696;">
                                    <td style="text-align:  center;color: #191919;font-weight:  bold;border-right: 1px solid #969696;">#' . $e['id'] . '</td>
                                    <td style="text-align:  center;">' . $e['Name']. '</td>
                                    <td style="font-weight: 500;padding-left: 15px;border-left: 1px solid #969696;border-right: 1px solid #969696;text-align:  left;">' . $e['mail'] . '</td>
                                    <td style="text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $e['phone'] . '</td>
                                    <td style="text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $e['city'] . '</td>
                                    <td style="text-align:  center;font-size:  18px;"> ('.$totals.') </td>
                                </tr>';
        $Orders .= '</tbody>
                    </table>';
        
        $Orders .='<table width="100%" style="width: 100% !important;"><tr><td style="vertical-align: super;width: 100%;">'.$table2.'</td></tr><tr><td style="vertical-align: super;width: 100%;">'.$table1.'</td></tr></table><td><tr><table>';
    }
$_HTML_ .=$Orders;
$_HTML_ .= '</div>
    </body>
</html>';//echo $_HTML_;exit();