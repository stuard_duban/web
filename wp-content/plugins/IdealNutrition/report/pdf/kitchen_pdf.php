<?php

$data = file_get_contents('https://'.$_SERVER['SERVER_NAME'].'/wp-content/plugins/IdealNutrition/report/json/kitchen.json');
$data = json_decode($data, true);

$_HTML_ = '<!DOCTYPE html>
<html style="margin: 0">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/bootstrap-table/src/bootstrap-table.css">
        <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
        <link rel="stylesheet" href="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/examples.css">
        <script src="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/jquery.min.js"></script>
        <script src="' . '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'Plegables/' . 'assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
        <![endif]-->
        <style>
            @font-face {
                font-family: "MankSans";
                src: url("https://ideal.reallibertychange.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.eot");
                src: local("☺"), url("https://ideal.reallibertychange.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.woff") format("woff"), url("https://ideal.reallibertychange.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.ttf") format("truetype"), url("https://ideal.reallibertychange.com/wp-content/themes/nb-foody-jruits/fonts/MankSans.svg") format("svg");
                font-weight: normal;
                font-style: normal;
            }
            body, h1, table, tr, td, th{font-family: "MankSans" !important;}
        </style>
    </head>
    <body>
        <div class="container">
            <div style="text-align:  center;"><img src="https://ideal.luxlifeentertainment.com/wp-content/uploads/2018/03/Logo-ideal-Nutritions-1.png" alt="IDEAL NUTRITION"></div>
            <br>
            <h1 style="font-size:  30px;text-transform:  uppercase;margin-bottom: 20px;">Kitchen Report</h1>
            <div style="text-align: right;width: 100%;">Printing Date: '.date('l, d, m, Y h:m a').'</div>
            <table cellpadding="0" cellspacing="0" style="width: 100%;border: 1px solid #1a1a1a75;">
                <thead>
                    <tr style="color: #353535;">
                        <th style="border-bottom: 1px solid #969696; padding:  5px;text-align: center;">REF</th>
                        <th style="border-bottom: 1px solid #969696; padding:  5px;text-align: center;">IMAGEN</th>
                        <th style="border-bottom: 1px solid #969696; padding:  5px;text-align: center;">NAME</th>
                        <th style="border-bottom: 1px solid #969696; padding:  5px;text-align: center;">QUANTITY</th>
                        <th style="border-bottom: 1px solid #969696; padding:  5px;text-align: center;">DAY</th>
                        <th style="border-bottom: 1px solid #969696; padding:  5px;text-align: center;">DELIVERY DATE</th>
                    </tr>
                </thead>
                <tbody>';
$rw = '';
/*echo "Data que obtiene: <pre>";
print_r($data);*/
$countItems = 0;
foreach ($data as $key => $e) {
    $cs = '';

    $color = "background-color: #DCDCDC";
    if($countItems % 2 == 0){
      $color = "background-color: #ffffff";
    }

    if(isset($e['custom']) && $e['custom'] != ""){
      //echo "Comidas custom: "; var_dump($e['custom']);
      $custom_meals = explode( '<hr style="margin: 3px;border: 1px solid #12e61a;">', $e['custom'] );
      //echo "Custom Meals: " . var_dump($custom_meals[0]);exit;

      $totalPlatillos = count($custom_meals);

      $medio = (int)($totalPlatillos / 2);
      //echo "Mitad  ". $medio;

      $countPlatillos = 0;
      foreach ($custom_meals as $plato) {
        //echo "<br>Platillo: <br>" . var_dump($plato);
        if($plato != "") {
          if($countPlatillos != $medio) {
            $cantidad = "";
          }else {
            $cantidad = $e['qty'];
          }

          if($countPlatillos < $totalPlatillos - 1) {
            //echo "Entra por aqui <br>"; . var_dump($plato);
            $rw .= '<tr style="border-top: 1px solid #969696;'.$color.'">
                                    <td style="text-align:  center;color: #191919;font-weight:  bold;border-right: 1px solid #969696;">' . $e['sku'] . '</td>
                                    <td style="text-align:  center;">' . str_replace('//', 'https://', $e['image']) . '</td>
                                    <td style="font-weight: 500;padding-left: 15px;border-left: 1px solid #969696;border-right: 1px solid #969696;text-align:  left;"><hr style="margin: 3px;border: 1px solid #12e61a;">' . $e['name'].'<br>'.$plato . '</td>
                                    <td style="text-align:  center;font-size:  18px;color: #ff0909;border-right: 1px solid #969696;">' . $cantidad . '</td>
                                    <td style="text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $e['day'] . '</td>
                                    <td style="text-align:  center;font-size:  18px;">' . $e['date'] . '</td>
                                </tr>';
          }
          else {
            $rw .= '<tr style="border-top: 1px solid #969696;'.$color.'">
                                    <td style="text-align:  center;color: #191919;font-weight:  bold;border-right: 1px solid #969696;">' . $e['sku'] . '</td>
                                    <td style="text-align:  center;">' . str_replace('//', 'https://', $e['image']) . '</td>
                                    <td style="font-weight: 500;padding-left: 15px;border-left: 1px solid #969696;border-right: 1px solid #969696;text-align:  left;"><hr style="margin: 3px;border: 1px solid #12e61a;">' . $e['name'].'<br>'.$plato . '<hr style="margin: 3px;border: 1px solid #12e61a;"></td>
                                    <td style="text-align:  center;font-size:  18px;color: #ff0909;border-right: 1px solid #969696;">' . $cantidad . '</td>
                                    <td style="text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $e['day'] . '</td>
                                    <td style="text-align:  center;font-size:  18px;">' . $e['date'] . '</td>
                                </tr>';
          }
        }
        $countPlatillos++;
      }

      //exit;
        //$cs = '<br>'.$e['custom'];
    }
    else {
      $rw .= '<tr style="border-top: 1px solid #969696;'.$color.'">
                            <td style="text-align:  center;color: #191919;font-weight:  bold;border-right: 1px solid #969696;">' . $e['sku'] . '</td>
                            <td style="text-align:  center;">' . str_replace('//', 'https://', $e['image']) . '</td>
                            <td style="font-weight: 500;padding-left: 15px;border-left: 1px solid #969696;border-right: 1px solid #969696;text-align:  left;">' . $e['name'].$cs . '</td>
                            <td style="text-align:  center;font-size:  18px;color: #ff0909;border-right: 1px solid #969696;">' . $e['qty'] . '</td>
                            <td style="text-align:  center;font-size:  18px;border-right: 1px solid #969696;">' . $e['day'] . '</td>
                            <td style="text-align:  center;font-size:  18px;">' . $e['date'] . '</td>
                        </tr>';
    }
    $countItems++;
}
$_HTML_ .= $rw;
$_HTML_ .= '</tbody>
            </table>
        </div>
    </body>
</html>';
