<?php
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_page-settings',
        'title' => 'Page settings',
        'fields' => array (
            array (
                'key' => 'field_591d1f0d76335',
                'label' => 'Title section',
                'name' => 'page_title_section',
                'type' => 'true_false',
                'instructions' => 'Turn on title section in this page or not',
                'message' => '',
                'default_value' => 0,
            ),
            array (
                'key' => 'field_59197beedbc73',
                'label' => 'Sidebar',
                'name' => 'page_sidebar',
                'type' => 'select',
                'choices' => array (
                    'left-sidebar' => 'Left sidebar',
                    'full-width' => 'Full width',
                    'right-sidebar' => 'Right sidebar',
                ),
                'default_value' => 'full-width',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_591d468e2f44c',
                'label' => 'Content width',
                'name' => 'page_content_width',
                'type' => 'number',
                'instructions' => 'Content area width in percent',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_59197beedbc73',
                            'operator' => '!=',
                            'value' => 'full-width',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => 70,
                'placeholder' => '',
                'prepend' => '',
                'append' => '%',
                'min' => 60,
                'max' => 80,
                'step' => 1,
            ),
            array (
                'key' => 'field_59197c59dbc74',
                'label' => 'Thumb',
                'name' => 'page_thumb',
                'type' => 'select',
                'instructions' => 'Display page thumbnail',
                'choices' => array (
                    'no-thumb' => 'None',
                    'standard' => 'Standard',
                ),
                'default_value' => 'no-thumb',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_59197cc7dbc76',
                'label' => 'Background color',
                'name' => 'page_bg_color',
                'type' => 'color_picker',
                'instructions' => 'This settings will not take effect if background image was set',
                'default_value' => '#ffffff',
            ),
            array (
                'key' => 'field_59197c98dbc75',
                'label' => 'Background Image',
                'name' => 'page_bg_image',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),
            array (
                'key' => 'field_59607556a20b9',
                'label' => 'Page Class',
                'name' => 'page_class',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}
