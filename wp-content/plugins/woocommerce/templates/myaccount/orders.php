<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */
if (!defined('ABSPATH')) {
    exit;
}

function getDeliveryPlegables($ID_Order) {
    global $wpdb;
    $table_name = $wpdb->prefix . "orders_in";
    $User = wp_get_current_user();
    $ID_User = $User->ID;
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=$ID_Order AND user=$ID_User");
    if (count($DB) === 0) {
        return false;
    } else {
        $DB = $DB[0];
    }
    return($DB);
}

function isSubscriptionsGreen($id) {
    $subscriptions = wcs_get_users_subscriptions();

    if ($subscriptions) {
        foreach ($subscriptions as $subscription) {
            //echo $subscription->get_data()['parent_id'].'==='.$id;
            if (floatval($subscription->get_data()['parent_id']) === floatval($id)) {
                return true;
            }
        }
    }
    return false;
}

function isPayAsYouGo($id) {
    $r = getDeliveryPlegables($id);
    if ($r) {
        if (count($r) > 0) {
            return true;
        }
    }
    return false;
}

do_action('woocommerce_before_account_orders', $has_orders);
?>

<?php if ($has_orders) : $modales = "";?>

    <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
        <thead>
            <tr>
                <?php foreach (wc_get_account_orders_columns() as $column_id => $column_name) : ?>
                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr($column_id); ?>"><span class="nobr"><?php echo esc_html($column_name); ?></span></th>
    <?php endforeach; ?>
            </tr>
        </thead>

        <tbody>
            <?php

            foreach ($customer_orders->orders as $customer_order) :
                $order = wc_get_order($customer_order);
                $item_count = $order->get_item_count();

                if(!isset($first_order_subs))
                {
                  global $wpdb;
                  $sql_suscripcion = "SELECT * FROM wp_orders_in WHERE orders = " . $order->get_order_number() . " ORDER BY first_delivery_date ASC";

                  $DB = $wpdb->get_results($sql_suscripcion);
                  $first_order_subs = $DB[0];
                  //var_dump($first_order_subs);
                  //exit;
                  $ordenes_suscripcion = array_splice($DB, 1);
                }



                /*echo "orden<pre>";
                var_dump($customer_order);*/
                ?>

                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr($order->get_status()); ?> order">
                        <?php foreach (wc_get_account_orders_columns() as $column_id => $column_name) : ?>

                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr($column_id); ?>" data-title="<?php echo esc_attr($column_name); ?>">
                            <?php if (has_action('woocommerce_my_account_my_orders_column_' . $column_id)) : ?>
                                <?php do_action('woocommerce_my_account_my_orders_column_' . $column_id, $order); ?>

                                <?php elseif ('order-number' === $column_id) : ?>
                                <a href="<?php echo esc_url($order->get_view_order_url()); ?>">
                                <?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?>
                                </a>

                            <?php elseif ('order-date' === $column_id) : ?>
                                <time datetime="<?php echo esc_attr($order->get_date_created()->date('c')); ?>"><?php
                                  if (isSubscriptionsGreen($order->get_order_number())) {

                                    echo date('F d, Y', strtotime($first_order_subs->order_date));
                                  }
                                  else {
                                    echo esc_html(wc_format_datetime($order->get_date_created()));
                                  } ?></time>

                            <?php elseif ('order-status' === $column_id) : ?>
                                <?php echo esc_html(wc_get_order_status_name($order->get_status())); ?>
                            <?php elseif ('delivery-info' === $column_id) : ?>

                                <?php
                                $rs = getDeliveryPlegables($order->get_order_number());
                                if (!$rs) {
                                    echo str_replace('$5', '', get_post_meta($order->get_id(), '_billing_myfield18', true));
                                    echo '<br>';
                                    $dat = get_post_meta($order->get_id(), '_billing_myfield18c', true);
                                    if ($dat === "free") {
                                        $dat = 'You will receive your meals on: <br><strong>' . get_post_meta($order->get_id(), '_billing_myfield16', true) . '</strong>';
                                    }
                                    echo $dat;
                                } else {
                                    /*echo $rs->city;
                                    echo '<br>';
                                    echo $rs->tim;*/

                                    echo $first_order_subs->city . "<br>";
                                    echo $first_order_subs->tim;
                                }
                                ?>
                            <?php elseif ('order-total' === $column_id) : ?>
                                <?php
                                /* translators: 1: formatted order total 2: total order items */
                                printf(_n('%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce'), $order->get_formatted_order_total(), $item_count);
                                ?>

                            <?php elseif ('order-actions' === $column_id) : ?>
                                <?php
                                $actions = wc_get_account_orders_actions($order);

                                if (!empty($actions)) {
                                    foreach ($actions as $key => $action) {
                                        echo '<a style="width: 100%;" href="' . esc_url($action['url']) . '" class="btn meal btn-primary btn-xs">' . esc_html($action['name']) . '</a>';
                                    }
                                }
                                if (isSubscriptionsGreen($order->get_order_number())) {
                                    ?><?php
                                } else {
                                    if (isPayAsYouGo($order->get_order_number())) {
                                        ?><a href="/view-meals-of-delivery/?order=<?= $order->get_order_number(); ?>&payasyougo=true" style="width: 100%;" class="btn meal btn-danger btn-xs"><?= esc_html('Delivery Info'); ?></a><?php
                                    }
                                }
                                ?>
                        <?php endif; ?>
                        </td>
                <?php endforeach; ?>
                </tr>

                <?php






                  if(count($ordenes_suscripcion) > 0) {
                    //Id boton modal
                    $numeroModal = -1;
                    foreach ($ordenes_suscripcion as $orden_suscripcion):
                      $numeroModal++;
                      $tim = explode('*', $orden_suscripcion->products);

                      $cant_suscripcion = explode(' - ', $tim[0]);

                      $cant = explode(',', $cant_suscripcion[count($cant_suscripcion) - 1]);

                      $modales .= "<div id='modal".$numeroModal."' class='modal-suscripcion' style=''>

                    				<!-- Modal content -->
                    				<div class='modal-suscripcion-content'>
                    					<span class='cerrar'>&times;</span>
                    					<table class='woocommerce-table woocommerce-table--order-details shop_table order_details'>

                    							<thead>
                    								<tr>
                    									<th class='woocommerce-table__product-name product-name'>Product</th>

                    								</tr>
                    							</thead>

                    							<tbody>
                    								<tr class='woocommerce-table__line-item order_item'>

                    						<td class='woocommerce-table__product-name product-name'>
                    							Subscription Plans - ";
                              $items_modal = "";
                              $total_suscripcion = 0;

                              foreach ($tim as $llave => $producto) {

                                if (strpos($producto, 'Subscription') !== false) {

                                  $partesProducto = explode(',', $producto);

                                  $total_suscripcion += $partesProducto[2];
                                  //var_dump($partesProducto);

                                  $items_modal .= "<tr class='woocommerce-table__line-item order_item'>

                        						<td class='woocommerce-table__product-name product-name'>".$partesProducto[1]."
                        							 <strong class='product-quantity'>× ".$partesProducto[2]."</strong>
                                    </td>
                                    </tr>";
                                }
                                else {
                                  break;
                                }
                              }





                            $modales .=  $total_suscripcion . " meals per week <strong class='product-quantity'>× 1</strong>	</td>

                          </tr>";

                            $modales .= $items_modal;



                    				$modales .=	"

                    							</tbody>

                    						</table>
                    				</div>

                    			</div>";

                          //Obtener el precio de las suscripcion
                          $productoTitulo = get_page_by_path( 'subscription-plans', OBJECT, 'product' );
                          //var_dump($productoTitulo);
                          $_pf = new WC_Product_Factory();

                          $_product = $_pf->get_product($productoTitulo->ID);

                          $variacionProducto = $_product->get_available_variations();



                          foreach ($variacionProducto as $indexpInd => $productoIndividual) {

                            //var_dump($productoIndividual["attributes"]["attribute_meals"]);
                            if($total_suscripcion . " meals per week" == $productoIndividual["attributes"]["attribute_meals"])
                            {
                              $precio_suscripcion = $productoIndividual["display_price"];

                              break;

                            }
                          }

                 ?>

                 <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr($order->get_status()); ?> order">
                         <?php foreach (wc_get_account_orders_columns() as $column_id => $column_name) : ?>

                         <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr($column_id); ?>" data-title="<?php echo esc_attr($column_name); ?>">
                             <?php if (has_action('woocommerce_my_account_my_orders_column_' . $column_id)) : ?>
                                 <?php do_action('woocommerce_my_account_my_orders_column_' . $column_id, $order); ?>

                                 <?php elseif ('order-number' === $column_id) : ?>

                                 <?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?>


                             <?php elseif ('order-date' === $column_id) : ?>
                                 <time datetime="<?php echo $orden_suscripcion->order_date; ?>"> <?php echo date('F d, Y', strtotime($orden_suscripcion->order_date)); ?> </time>

                             <?php elseif ('order-status' === $column_id) : ?>
                                 <?php echo esc_html(wc_get_order_status_name($order->get_status())); ?>
                             <?php elseif ('delivery-info' === $column_id) : ?>

                                 <?php
                                   echo $orden_suscripcion->city . "<br>";
                                   echo $orden_suscripcion->tim;
                                 ?>
                             <?php elseif ('order-total' === $column_id) : ?>
                                 <?php
                                 echo "$" . $precio_suscripcion . " for ".$total_suscripcion."  items";
                                 /* translators: 1: formatted order total 2: total order items */
                                 //printf(_n('%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce'), $order->get_formatted_order_total(), $item_count);
                                 ?>

                             <?php elseif ('order-actions' === $column_id) : ?>
                                 <?php
                                 $actions = wc_get_account_orders_actions($order);

                                 if (!empty($actions)) {
                                     foreach ($actions as $key => $action) {
                                         echo '<a style="width: 100%;" class="btn meal btn-primary btn-xs boton-modal-suscripcion" id="'. $numeroModal .'">' . esc_html($action['name']) . '</a>';
                                     }
                                 }

                                 ?>
                         <?php endif; ?>
                         </td>
                 <?php endforeach; //echo "se genero modal:" . $modales;?>
                 </tr>

               <?php endforeach; }?>

    <?php endforeach; ?>
        </tbody>
    </table>

    <?php echo $modales; ?>
    <?php do_action('woocommerce_before_account_orders_pagination'); ?>

        <?php if (1 < $customer_orders->max_num_pages) : ?>
        <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
            <?php if (1 !== $current_page) : ?>
                <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page - 1)); ?>"><?php _e('Previous', 'woocommerce'); ?></a>
            <?php endif; ?>

            <?php if (intval($customer_orders->max_num_pages) !== $current_page) : ?>
                <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page + 1)); ?>"><?php _e('Next', 'woocommerce'); ?></a>
        <?php endif; ?>
        </div>
    <?php endif; ?>

        <?php else : ?>
    <div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
        <a class="woocommerce-Button button" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
        <?php _e('Go shop', 'woocommerce') ?>
        </a>
    <?php _e('No order has been made yet.', 'woocommerce'); ?>
    </div>
<?php endif; ?>

<?php do_action('woocommerce_after_account_orders', $has_orders); ?>
