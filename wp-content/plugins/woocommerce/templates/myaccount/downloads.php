<?php
/**
 * Downloads
 *
 * Shows downloads on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */
/*ini_set('display_errors', 1);
error_reporting(E_ALL);*/
if (!defined('ABSPATH')) {
    exit;
}
$downloads = WC()->customer->get_downloadable_products();
$has_downloads = (bool) $downloads;
?><h1>Plan Subscription</h1><?php
$User = get_currentuserinfo();
$subscriptions = wcs_get_users_subscriptions();
if ($subscriptions) {
    ?>
    <table cellspacing="0" cellpadding="0"  style="position:relative;width: 100%; margin: 0 auto;">
        <thead>
            <tr>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number">
                    #Orden
                </th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                    Plan
                </th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                    Meals Selected for the week?
                </th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                    Action
                </th>
            </tr>
        </thead>
        <tbody>
            <?php

            foreach ($subscriptions as $subscription) {
              /*echo "estas son las suscripciones<pre>";
            var_dump($subscriptions);echo "<br><br>";//exit;*/
                if (esc_attr(wcs_get_subscription_status_name($subscription->get_status())) == 'Active') {
                    ?>
                    <tr>
                        <?php $order_id = $subscription->get_data()['parent_id']; ?>
                        <td>
                            <a href="<?= '/my-account/view-order/' . $order_id; ?>"><?= $order_id; ?></a>
                        </td>
                        <?php
                        $name = '';
                        foreach ($subscription->get_items() as $line_item) {
                            ?>
                            <td>
                                <?=
                                $line_item['name'];
                                $name = str_replace('', '', str_replace(' meals per week', '', explode('- ', $line_item['name'])[1]));
                                ?>
                            </td>
                        <?php } ?>
                        <td style="text-align:  center;">
                            <?php if (!SelectMealsbyOrder($order_id)) { ?>
                                NO
                            <?php } else { ?>
                                YES
                            <?php } ?>
                        </td>
                        <td style="min-width: 276px;text-align:  right;">
                            <?php //var_dump(SelectMealsbyOrder($order_id));exit();?>
                            <?php if (!SelectMealsbyOrder($order_id)) { ?>
                                <a href="/select-your-meals/?order=<?= $order_id; ?>&plan" class="btn meal btn-warning btn-xs"><?= esc_html('See Meals'); ?></a>
                            <?php } ?>
                            <a href="/your-order-of-the-week/?order=<?= $order_id; ?>" class="btn meal btn-primary btn-xs"><?= esc_html('View Order'); ?></a>
                            <a href="/view-meals-of-delivery/?order=<?= $order_id; ?>" class="btn meal btn-danger btn-xs"><?= esc_html('Delivery Info'); ?></a>
                        </td>
                    </tr><?php } ?>

            <?php } ?>
        </tbody>
    </table>
<?php } else {
    ?>
    <div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
        <a class="woocommerce-Button button" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
            <?php esc_html_e('Go shop', 'woocommerce') ?>
        </a>
        <?php esc_html_e('You do not have any active subscription plan.', 'woocommerce'); ?>
    </div>
<?php } ?>
<?php do_action('woocommerce_after_account_downloads', $has_downloads); ?>
